(function() {
    'use strict';

    var app = angular.module('TableCmp', ['ui.bootstrap' ]);

    app.controller('TableCmpCtrl', ['$scope', function($scope) {
        var vm = this;
        $scope.some = "print something";

        vm.structure = {
            columns: [{
                title: 'ABC',
                column: 'abc',
                type: 'string',
                typeAhead: 'states'
            }, {
                title: 'DEF',
                column: 'def',
                type: 'string',
                typeAhead: ''
            }, {
                title: 'XYZ',
                column: 'x',
                type: 'date',
                typeAhead: ''
            }],
            title: 'Sample Table'
        };
        vm.data = [{
            abc: '',
            def: 'XXXX',
            x: new Date()
        }, {
            abc: 'DEDD',
            def: 'XX',
            x: new Date()
        }, {
            abc: 'CD',
            def: 'X',
            x: new Date()
        }, {
            abc: 'A',
            def: 'XYX',
            x: new Date()
        }, {
            abc: 'ABCDEF',
            def: 'XZ',
            x: new Date()
        }];

        vm.addNewHandler = function() {
            vm.data.push({ "abc": '', "def": '', "x": new Date() });
        };

        vm.editHandler = function(entity) {
            
            console.log('editing : ', entity);
        };

        vm.viewHandler = function(entity) {
            console.log('viewing: ', entity);
        };
    }]);

    app.component('genericTable', {
        templateUrl: 'templates/tableUI.html',
        controller: tableController,
        bindings: {
            structure: '<',
            data: '=',
            addNewHandler: '&addHandler',
            editHandler: '&',
            viewHandler: '&'
        }
    });

    function tableController($scope, $uibModal, tservice) {
        /* @ng-valid: this */
        var vm = this;

        //add component



        // $scope.states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Dakota', 'North Carolina', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];

        vm.add = function() {

            console.log('addCallback');
            console.log(vm.structure);
            //create modal to get the data
             $uibModal.open({
                animation: true,
                templateUrl:'templates/modalTemplate.html',
                controller: function(dataa){
                    $scope.data = dataa.columns[0].title;
                    console.log($scope.data);

                },
                resolve:{
                    dataa:function(){
                        return vm.structure;
                    }
                }

            });
            console.log("after modal");

            if (vm.addNewHandler) {
                vm.addNewHandler();
            }
        };

        vm.edit = function(dataRow) {
            console.log('editCallback');
            if (vm.editHandler) {
                vm.editHandler({ entity: dataRow });
            }
        };

        vm.view = function(dataRow) {
            console.log('viewCallback');
            if (vm.viewHandler) {
                vm.viewHandler({ entity: dataRow });
            }
        };

        vm.openModal = function() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '../templates/modalTemplate.html',
                controller: modalController,
                controllerAs: 'mctrl',
                resolve: {
                    data: function() {
                        if (ctl.test) {
                            console.log(ctl.test);
                            return ctl.test;
                        }
                    }
                }
            });
            modalInstance.result.then(function(obj) {
                ctl.savedData = tservice.getData();

            }, function() {

            });


        };
    }

    function modalController($uibModalInstance, $scope, data, tservice) {
        var vm = this;
        vm.pam = data;
        vm.zoom = data;
        //console.log(data);
        vm.save = function() {
            //console.log(vm.pam);
            tservice.save(vm.pam);
            $uibModalInstance.close(vm.pam);
        };


    }


    /*app.component('rows', {
        templateUrl: '../templates/rows.html',
        controller: rowsController

    });

    function rowsController(tservice) {
        var rctl = this;
        rctl.savedData = tservice.getData();



    }
*/




})();