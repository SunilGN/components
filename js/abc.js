(function() {
    'use strict';

    angular
        .module('module')
        .controller('Controller', Controller);

    Controller.$inject = ['dependencies'];

    /* @ngInject */
    function Controller(dependencies) {
        var vm = this;
        vm.title = 'Controller';

        activate();

        ////////////////

        function activate() {
        }

    }
})();
(function() {
    'use strict';

    angular
        .module('module')
        .service('Service', Service);

    Service.$inject = ['dependencies'];

    /* @ngInject */
    function Service(dependencies) {
        this.func = func;

        ////////////////

        function func() {
        }
    }
})();
(function() {
    'use strict';

    angular
        .module('module')
        .component('component', {
            bindings: {

            },
            controller: Controller

        });

    Controller.$inject = ['dependencies'];

    /* @ngInject */
    function Controller(dependencies) {

    }
})();